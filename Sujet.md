Sujet 3 - Informatisation de la gestion de spectacles

Une société gérants des évènements du type concerts, spectacles, meeting aériens… veut informatiser ses activités. 
Cette société met en relation des organisateurs d'événements, des propriétaires de lieux où organiser des événements et 
les spectateurs. L'application devra permettre, entre autre :

* à un propriétaire de lieux où organiser un événement de s'inscrire dans l'application, de mettre à disposition un ou 
plusieurs lieux, de décrire les lieux (capacité d'accueil, informations sur le lieux…), de déterminer les dates de 
disponibilités, de consulter les locations…
* à un organisateur d'événement de chercher un lieu disponible, de créer un événement, de fixer des prix pour les 
tickets, de donner des renseignements sur l'événement, de consulter la vente des tickets…De plus, le jour de 
l'événement, l'application permettra de contrôler les billets soit par scan, soit par saisie du numéro. Il faut bien 
entendu gérer les cas de fraudes, comme par exemple un ticket imprimé plusieurs fois, ou les faux tickets.
* à un spectateur de choisir un événement, de s'enregistrer sur l'application, d'acheter un ticket, d'imprimer son 
ticket ou de le stocker sur son téléphone, de gérer ses tickets.
* à la société gérant les événements de facturer ses services, de consulter les comptes des clients…

Les fonctionnalités de l'application sont à affiner et à détailler (scénarios et cas d'utilisations)
À propos de la facturation

Il faut réfléchir au modèle de facturation.

* Pour un propriétaire de lieu, cela peut être soit un pourcentage sur le prix demandé pour la location, soit un 
abonnement d'un an avec un tarif fixé. Quand l'abonnement prend fin, le lieu n'est plus disponible.
* Pour un organisateur, cela peut être un prix fixe pour chaque spectacle en fonction du nombre de spectateurs attendus.
* Pour un spectateur, cela peut être un prix fixe par ticket.
