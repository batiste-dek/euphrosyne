# Euphrosyne

**Objectif :** Euphrosyne est une solution informatique pour la gestion des spectacles.

**Description:** Euphrosyne est un logiciel de gestion des spectacles qui permet à plusieurs personnes de profiter pleinement de
l'expérience du spectacle. Que vous soyez un spectateur, un organisateur ou encore un propriétaire de lieu, Euphrosyne
a été conçu pour vous. Euphrosyne vous permet d'organiser des spectacles si vous êtes un organisateur, de proposer vos
lieux si vous êtes propriétaire ou d'acheter des billets si vous êtes un spectateur.

## User stories

```include
UserStories.md
```

## Cas d'utilisations

![Diagramme de cas d'utilisations](images/UseCaseDiagrams/GeneralUseCaseDiagram_v1.3.PNG)

- Cas 1: Gestion des membres

  - Description: Relatif à l'authentification et les comptes clients
  - Acteurs: Membre
  - Scénarios:
    ```include
    scenarios/ScenarioConnexion.md
    scenarios/ScenarioInscription.md
    ```
    ![DSS Inscription](images/SequentialDiagram/Inscription_DSS.png)
    ![DSD Inscription](images/SequentialDiagram/Inscription_DSD.png)

    - Scenario Désinscription
    
![Diagramme de cas d'utilisations de la Gestion des membres](images/UseCaseDiagrams/AuthentificationUseCaseDiagram_v1.0.PNG)

- Cas 2: Gestion des lieux

  - Description: Relatif à la gestion des lieux
  - Acteurs: Propriétaire
  - Scénarios:
    - Scenario Créer Lieu
    ```include
    scenarios/ScenarioProposerLieu.md
    ```

    ![DSS Inscription](images/SequentialDiagram/Proposer_lieu_DSS.png)
    ![DSD Inscription](images/SequentialDiagram/Proposer_lieu_DSD-Page-1.png)

    - Scenario Retirer Lieu
    - Scenario Donner Disponibilités

![Diagramme de cas d'utilisations de la Gestion des lieux](images/UseCaseDiagrams/GererUnLieuUseCaseDiagram_v1.0.PNG)

- Cas 3: Gestion des locations

  - Description: Relatif à la gestion des locations
  - Acteurs: Propriétaire
  - Scénarios:
    ```include
    scenarios/ScenarioConfirmerLocation.md
    ```
    ![DSS Inscription](images/SequentialDiagram/AccepterReservation_dss.png)
    ![DSD Inscription](images/SequentialDiagram/AccepterReservation_dsd.png)
    - Scenario Modification Location
    - Scenario Suppression Location
    - Scenario Consulter Locations

![Diagramme de classe spécifique](images/ClassDiagrams/GestionLocation_v1.1.PNG)

- Cas 4: Gestion d'événement

  - Description: Relatif à la gestion des événements
  - Acteurs: Organisateur
  - Scénarios:
    ```include
    scenarios/ScenarioOrganisationEvenement.md
    ```

    ![DSS Inscription](images/SequentialDiagram/OrganiserEvenement_dss.png)
    ![DSD Inscription](images/SequentialDiagram/OrganiserEvenement_dsd.png)

    - Scenario Modification Evenement
    - Scenario Suppression Evenement

![Diagramme de cas d'utilisations de la Gestion des évènements](images/UseCaseDiagrams/GererLesEvenementsUseCaseDiagram_v1.0.PNG)
![Diagramme de classe spécifique](images/ClassDiagrams/GestionEvenement_v1.1.PNG)

- Cas 5: Recherche d'événement

  - Description: Relatif à la recherche d'événements
  - Acteurs: Spectateur
  - Scénarios:
    Scenario Recherche Evenement
    ```include
    scenarios/ScenarioRechercheBillet.md
    ```

    ![DSS Inscription](images/SequentialDiagram/AcheterBillet_dss.png)
    ![DSD Inscription](images/SequentialDiagram/AcheterBillet_dsd.png)
    - Scenario Remboursement Billet

    ![DSS Inscription](images/SequentialDiagram/AnnulerBillet_dss.png)
    ![DSD Inscription](images/SequentialDiagram/AnnulerBillet_dsd.png)
    - Scenario Impression Billet
    - Scenario Gestion Tickets

- Cas 6: Validation de billet

  - Description: Relatif à la validation des billets
  - Acteurs: Spectateur
  - Scénarios:
    - Scenario Validation Billet

    ![DSS Inscription](images/SequentialDiagram/ValidationBillet_dss.png)
    ![DSD Inscription](images/SequentialDiagram/ValidationBillet_dsd.png)

    ```include
    scenarios/ScenarioBilletFraude.md
    ```

    ![DSS Inscription](images/SequentialDiagram/ValidationBilletFraude_dss.png)
    ![DSD Inscription](images/SequentialDiagram/ValidationBilletFraude_dsd.png)

![Diagramme de cas d'utilisations de la Validation des tickets](images/UseCaseDiagrams/ValiderSonTicketUseCaseDiagram_v1.0.PNG)
![Diagramme de classe spécifique](images/ClassDiagrams/ValidationBillet_v1.1.PNG)

- Cas 7: Administration
  - Description: Relatif à la société gérant le service
  - Acteurs: Administrateur
  - Scénarios:
    - Scenario Facturation Service

    ![DSS Inscription](images/SequentialDiagram/Service_facturation_dss-Page-1.png)
    ![DSD Inscription](images/SequentialDiagram/Service_facturation_dsd.png)

    - Scenario Gestion Clients

## Diagramme de classes

![Diagramme de classe global](images/ClassDiagrams/euphrosine_diagram_class_v1.3.PNG)


## Tableau des concepts

| Concept                             | Types                              | Cas d'utilisations appliquées                   |
| ----------------------------------- | ---------------------------------- | ----------------------------------------------- |
| Abonnement                          | Données                            | Louer son lieu                                  |
| Évènement                           | Données                            | Créer un évènement                              |
| Lieu                                | Données                            | Louer son lieu                                  |
| Billet                              | Données                            | Acheter un ticket pour un évènement             |
| Spectateur                          | Données                            | Acheter un ticket                               |
| Organisateur                        | Données                            | Chercher un lieu disponible, Créer un évènement |
| Propriétaire                        | Données                            | Louer son lieu                                  |
| Facturation                         | Données                            | Créer un évènement, louer son lieu              |
| Créer un évènement                  | Fonctionnalité / Cas d'utilisation | Créer un évènement                              |
| Consulter la vente des tickets      | Fonctionnalité / Cas d'utilisation | Voir les informations de l'évènement            |
| Contrôler les billets               | Fonctionnalité / Cas d'utilisation | Contrôler les billets                           |
| Acheter un ticket pour un évènement | Fonctionnalité / Cas d'utilisation | Acheter un ticket pour un évènement             |
| Consulter les évènements            | Fonctionnalité / Cas d'utilisation | Chercher un évènement                           |
| Metre à disposition un lieu         | Fonctionnalité / Cas d'utilisation | Inscrire un lieu                                |
| Chercher un lieu                    | Fonctionnalité / Cas d'utilisation | Chercher un lieu                                |
| Consulter ses billets               | Fonctionnalité / Cas d'utilisation | Consulter ses billets                           |

## Glossaire

- Abonnement, un abonnement pour un propriétaire de lieu, cela lui permet de louer son lieu à des organisateurs.
- Capacité d'accueil, le nombre de personnes qu'un lieu peut héberger.
- Comptes des membres, le compte des membres.
- Date de disponibilité, la date de disponibilité d'un lieu.
- Évènement, un évènement est créer par un organisateur et des places sont vendues pour le spectateur.
- Facturation, la facturation d'un billet, ou de la location d'une salle.
- Fraude, lorsqu'un client essaie de valider un ticket invalide ou qui a déjà été validée.
- Lieu, le lieu dans laquel à lieu un évènement.
- Location, louer un lieu.
- Organisateur, la personne qui s'occupe d'organiser un évènement.
- Prix, le prix d'un billet, ou d'une location de salle.
- Propriétaire de lieu, la personne qui loue son lieu pour des spectacles.
- Renseignements sur l'évènement
- Scan, lorsqu'on scan le billet à l'entrée de l'évènement.
- Service, c'est l'accès à la plateforme.
- S'enregistrer, s'inscrire sur le site.
- Spectateur, un client qui a acheté un billet et qui participe à l'évènement.
- Tarif, c'est lorsqu'on loue son lieu pendant une période donnée avec un prix fixe, donc sans abonnement.
- Ticket, un billet vendu pour un évènement.
- Vente de tickets, le lien de la page où on veut un ticket pour un évènement.
